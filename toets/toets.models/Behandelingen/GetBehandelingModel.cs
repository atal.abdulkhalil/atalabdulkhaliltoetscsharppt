﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace toets.models.Behandelingen
{
    public sclass GetBehandelingModel
    {
        public Guid Id { get; set; }

        public Guid PatientId { get; set; }
        public string Patient { get; set; }

        public Guid SpecialistId { get; set; }
        public string Specialist { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Start { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Einde { get; set; }
    }
}
