﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace toets.api.Entities
{
    public class ToetsDbContext : DbContext
    {
        public DbSet<Patient> Patienten { get; set; }
        public DbSet<Behandeling> Behandelingen { get; set; }
        public DbSet<Specialist> Specialisten { get; set; }


        public ToetsDbContext(DbContextOptions<ToetsDbContext> options) : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            if (builder == null) { throw new ArgumentNullException(nameof(builder)); }

            builder.Entity<Patient>(b =>
            {
                b.HasMany(e => e.Behandelingen)
                .WithOne(e => e.Patient)
                .HasForeignKey(e => e.PatientId)
                .IsRequired();

            });

            builder.Entity<Behandeling>(b =>
            {
                b.HasOne(e => e.Patient)
                .WithMany(e => e.Behandelingen)
                .HasForeignKey(e => e.PatientId)
                .IsRequired();

                b.HasOne(e => e.Specialist)
               .WithMany(e => e.Behandelingen)
               .HasForeignKey(e => e.SpecialistId)
               .IsRequired();
            });



        }



    }
}
