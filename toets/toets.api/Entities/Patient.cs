﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace toets.api.Entities
{
    public class Patient
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(45, MinimumLength = 2)]
        public string Naam { get; set; }

        [Required]
        [StringLength(45, MinimumLength = 2)]
        public string Adres { get; set; }

        [Required]
        [StringLength(45, MinimumLength = 2)]
        public string Plaats { get; set; }

        [Required]
        public int ZiekenfondsNr { get; set; }

        public ICollection<Behandeling> Behandelingen { get; set; }
    }
}
