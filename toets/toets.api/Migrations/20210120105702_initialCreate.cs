﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace toets.api.Migrations
{
    public partial class initialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Patienten",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false),
                    Adres = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false),
                    Plaats = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false),
                    ZiekenfondsNr = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patienten", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Specialisten",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false),
                    Afdeling = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specialisten", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Behandelingen",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PatientId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SpecialistId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Start = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Einde = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Behandelingen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Behandelingen_Patienten_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patienten",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Behandelingen_Specialisten_SpecialistId",
                        column: x => x.SpecialistId,
                        principalTable: "Specialisten",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Behandelingen_PatientId",
                table: "Behandelingen",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Behandelingen_SpecialistId",
                table: "Behandelingen",
                column: "SpecialistId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Behandelingen");

            migrationBuilder.DropTable(
                name: "Patienten");

            migrationBuilder.DropTable(
                name: "Specialisten");
        }
    }
}
